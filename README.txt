INTRODUCTION
------------

This module allows you to transfer stock and price data between an        
Ubercart online store and text files on your desktop computer in          
comma-separated-values (CSV) format. CSV files can be easily read and     
written by spreadsheet applications. It can export product prices,        
stock levels and product option prices to CSV files and import updated    
versions of those same files to adjust prices and stock levels.     

 * For a full description of the module, visit the project page:
   https://drupal.org/project/uc_stock_update
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/uc_stock_update

Features:

 * Writes and reads RFC-4180 compliant CSV files (unlike code based on
   PHP's fgetcsv() function, which doesn't handle quote escaping
   correctly).

 * Uses column headings to identify columns. This allows you to
   reorder or add new columns in the downloaded CSV file before
   importing it back into your store. It also guards against
   accidentally importing the wrong CSV file.

 * Updates prices only for the current revision of a product, leaving
   prices for other revisions alone.

 * Handles both product prices and product option prices


REQUIREMENTS
------------
This module requires the following modules:
 * Ubercart (https://drupal.org/project/ubercart)


INSTALLATION
------------

 1) Install as you would normally install a contributed drupal
    module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

 2) Log in as the site administrator.

 3) Choose "Modules" (admin/modules) from the main toolbar and enable
    the "Stock and Price Updater" module.

 3) Follow the "Permissions" link next to the module name.  Assign
    "Import stock and price data from CSV files" permission to any
    roles that require it.

USAGE
-----

Choose "Store" from the main toolbar (admin/store).
Scroll down to the PRODUCTS section.
Follow the link labelled "Update stock and price data from CSV files".
Follow the instructions on that page (admin/store/products/update-stock-csv).


EXTENDING
---------

This module can be extended by writing a module that implements
hook_uc_stock_update_handlers(). See
./uc_stock_update.module
./UcStockUpdate.inc

